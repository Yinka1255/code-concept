<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$title_for_layout =('Login | Codes Concept Ltd Test');


?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
			<title>
				<?php echo $title_for_layout; ?>
			</title>
	
	 

	

		<?php		
		
		echo $this->Html->css('assets/lib/bootstrap/css/bootstrap');
		
		echo $this->Html->css('assets/lib/font-awesome/css/font-awesome');
		
		echo $this->Html->css('assets/css/main');
		
		echo $this->Html->css('assets/lib/metismenu/metisMenu');
		
		echo $this->Html->css('assets/lib/onoffcanvas/onoffcanvas');
		
		echo $this->Html->css('assets/lib/animate.css/animate');
		
		echo $this->Html->css('index');
		
		
		echo $this->Html->script('assets/lib/jquery/jquery');
		
		echo $this->Html->script('assets/lib/bootstrap/js/bootstrap');

		
		echo $this->Html->meta ('favicon', array('type'=>'ico'));
		
		echo $this->fetch('meta');

		echo $this->fetch('script');

		echo $this->fetch('css');

		?>

	</head>
<body class="login">
    <div id="container">
		
		<div id="content">
			<div id="flashMessage">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<?php echo $this->Session->flash(); ?>
					</div>
				</div>
			</div>		
			<?php echo $this->fetch('content'); ?>
			
			
		</div>
		
	</div>
<script>
	$(document).ready(function() {
	$('#flashMessage').delay(3000).fadeOut(200);
});
</script>
	</body>
</html>
