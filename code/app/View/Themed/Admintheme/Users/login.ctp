
<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="author" content="Yinka Adeniran">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
   


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="login">

      <div class="form-signin">
    <div class="text-center">
		<?php echo $this->Html->link('Africa Codes Concept Test', ['controller'=>'Users', 'action'=>'login'], ['class'=>'logo']);?>
    </div>
    <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
            <?php echo $this->Form->create('User');?>
                <?php echo $this->Form->input('username', array('placeholder'=>'Email', 'class'=>'form-control', 'label'=>false)); ?> 
                <?php echo $this->Form->input('password', array('placeholder'=>'Password', 'class'=>'form-control', 'type'=>'password', 'label'=>false)); ?>
                <div class="checkbox">
		  <label>
		    <input type="checkbox"> Remember Me
		  </label>
		</div><?php echo $this->Form->submit('Login', array('class'=>'btn btn-lg btn-primary btn-block', 'formnovalidate'=>true )); echo $this->Form->end(); ?>
  </div>


    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('.list-inline li > a').click(function() {
                    var activeForm = $(this).attr('href') + ' > form';
                    //console.log(activeForm);
                    $(activeForm).addClass('animated fadeIn');
                    //set timer to 1 seconds, after that, unload the animate animation
                    setTimeout(function() {
                        $(activeForm).removeClass('animated fadeIn');
                    }, 1000);
                });
            });
        })(jQuery);
    </script>
</body>

</html>
