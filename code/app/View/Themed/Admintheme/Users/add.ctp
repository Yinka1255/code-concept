<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
    
  


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

   
  </head>

        <body>
            <div class="bg-dark dk" id="wrap">
                <div id="top">
                    <!-- .navbar -->
                    <nav class="navbar navbar-inverse navbar-static-top">
                        <div class="container-fluid">
                    
                    
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <header class="navbar-header">
                    
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse ">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <?php echo $this->Html->link('Test', ['controller'=>'Users', 'action'=>'index'], ['class'=>'logo']);?>
                    
                            </header>
                    
                    
                    
                            
                    
                    
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                                <!-- .nav -->
                                <ul class="nav navbar-nav">
                                    <li><?php echo $this->Html->link('Dashboard', ['controller'=>'Users', 'action'=>'index']);?></li>
                                    <li class='dropdown '>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            Admin Panel <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><?php echo $this->Html->link('Add Admin', ['controller'=>'Users', 'action'=>'add']);?></li>
                                            <li><?php echo $this->Html->link('Edit Admin', ['controller'=>'Users', 'action'=>'edit']);?></li>
											
                                        </ul>
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                    <!-- /.navbar -->
                       
                </div>
                <!-- /#top -->
                    <div id="left">
                        <div class="media user-media bg-dark dker">
                            <div class="user-media-toggleHover">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="user-wrapper bg-dark">
                                <a class="user-link" href="">
								<?php if(empty($userinfo['Admin']['image'])){
									$userinfo['Admin']['image'] = 'user.jpg';
									}
								?>	
									<?php echo $this->Html->image('/img/'.$userinfo['Admin']['image'], array('width'=>'60px',"class"=>"media-object img-thumbnail user-img", "alt"=>"User Picture"));?>
                                </a>
                        
                                <div class="media-body">
                                    <span class="glyphicon glyphicon-user"></span> Hi, <?php echo $userinfo['Admin']['name'];?>
                                    <ul class="list-unstyled user-info">
                                        <li><span class="glyphicon glyphicon-cog"></span><?php echo $this->Html->link(' My Account', ['controller'=>'Users', 'action'=>'myaccount'],['id'=>'account_link']);?> </li>
										<li><span class="glyphicon glyphicon-share-alt"></span><?php echo $this->Html->link(' logout', ['controller'=>'Users', 'action'=>'logout'],['id'=>'account_link']);?> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- #menu -->
                        <ul id="menu" class="bg-blue dker">
                            <li class="nav-header">Menu</li>
                                <li class="nav-divider"></li>
                                  <li class="">
                                    <?php echo $this->Html->link('Dashboard', ['controller'=>'Users', 'action'=>'index']);?>
                                    
                                  </li>
                                  <li class="">
                                    <?php echo $this->Html->link('New Customer', ['controller'=>'Customers', 'action'=>'add']);?>
                                  </li>
								  <li class="">
                                    <?php echo $this->Html->link('Customers', ['controller'=>'Customers', 'action'=>'customers']);?>
                                  </li>
								  <li class="">
                                    <?php echo $this->Html->link('Transaction', ['controller'=>'Transactions', 'action'=>'transactions']);?>
                                  </li>
								  
								  
								  
                        </ul>
                        <!-- /#menu -->
                    </div>
                    <!-- /#left -->
                <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter" style="min-height:650px;">
							<div class="row">
								<div class="col-lg-9">
									<div class="box dark">
										<header>
											
											<h5>Add Admin</h5>
											
										</header>
										<div id="div-1" class="body">
												<?php echo $this->Form->create('User', ['class'=>'form-horizontal']);?>
												
												<div class="form-group">
													<label for="text1" class="control-label col-lg-4">Name</label>

													<div class="col-lg-8">
														<?php echo $this->Form->input('name', array('placeholder'=>'Name', 'class'=>'form-control', 'label'=>false)); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="text1" class="control-label col-lg-4">Username</label>

													<div class="col-lg-8">
														<?php echo $this->Form->input('username', array('placeholder'=>'Email', 'class'=>'form-control', 'label'=>false, 'type'=>'email')); ?>
													</div>
												</div>
												<div class="form-group">
													<label for="pass1" class="control-label col-lg-4">Password</label>

													<div class="col-lg-8">
														<?php echo $this->Form->input('password', array('class'=>'form-control', 'label'=>false, 'type'=>'password')); ?>
													</div>
													
												</div>
												
												<div class="form-group">
													<label class="control-label col-lg-4"> </label>
													<div class="col-lg-4 col-offset-lg-4">
														<?php echo $this->Form->submit('Create Admin', array('class'=>'btn btn-lg btn-success btn-block', 'formnovalidate'=>true)); echo $this->Form->end(); ?>
													</div>
												</div>		
												<!-- /.form-group -->
											
										</div>
									</div>
								</div>
							</div>


						</div>

					</div>
				</div>
			</div>	
            <!-- /#wrap -->
						<footer class="Footer bg-dark dker">
							<p>2017 &copy; NIX Global</p>
						</footer>
				
            </div>
            <!-- /.modal -->
            <!-- /#helpModal -->
           
                    

                

        </body>

</html>

<script>
  $( function() {
    $( "#date" ).datepicker({ maxDate: new Date});
  } );
 </script>
