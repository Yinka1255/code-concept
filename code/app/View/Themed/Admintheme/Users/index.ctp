<?php
	$jancount = 0;
	$jan = 0;
	foreach($jan_count as $jan_count){
		$jan++;
		$jancount += $jan_count['Transaction']['deposit'];
	}
	$febcount = 0;
	$feb = 0;
	foreach($feb_count as $feb_count){
		$feb ++;
		$febcount += $feb_count['Transaction']['deposit'];
	}
	$marcount = 0;
	$mar = 0;
	foreach($mar_count as $mar_count){
		$mar ++;
		$marcount += $mar_count['Transaction']['deposit'];
	}
	$aprcount = 0;
	$apr = 0;
	foreach($apr_count as $apr_count){
		$apr ++;
		$aprcount += $apr_count['Transaction']['deposit'];
	}
	$maycount = 0;
	$may = 0;
	foreach($may_count as $may_count){
		$may ++;
		$maycount += $may_count['Transaction']['deposit'];
	}
	$juncount = 0;
	$jun = 0;
	foreach($jun_count as $jun_count){
		$jun ++;
		$juncount += $jun_count['Transaction']['deposit'];
	}
	$julcount = 0;
	$jul = 0;
	foreach($jul_count as $jul_count){
		$jul ++;
		$julcount += $jul_count['Transaction']['deposit'];
	}
	$augcount = 0;
	$aug = 0;
	foreach($aug_count as $aug_count){
		$aug ++;
		$augcount += $aug_count['Transaction']['deposit'];
	}
	$sepcount = 0;
	$sep = 0;
	foreach($sep_count as $sep_count){
		$sep ++;
		$sepcount += $sep_count['Transaction']['deposit'];
	}
	$octcount = 0 ;
	$oct = 0;
	foreach($oct_count as $oct_count){
		$oct ++;
		$octcount += $oct_count['Transaction']['deposit'];
	}
	$novcount = 0;
	$nov = 0;
	foreach($nov_count as $nov_count){
		$nov ++;
		$novcount += $nov_count['Transaction']['deposit'];
	}
	$deccount = 0;
	$dec = 0;
	foreach($dec_count as $dec_count){
		$dec ++;
		$deccount += $dec_count['Transaction']['deposit'];
	}
	
?>
<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
    <meta name="author" content="">
    
    <meta name="msapplication-TileColor" content="#5bc0de" />
    <meta name="msapplication-TileImage" content="assets/img/metis-tile.png" />
    
  


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

   <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  </head>

        <body>
            <div class="bg-dark dk" id="wrap">
                <div id="top">
                    <!-- .navbar -->
                    <nav class="navbar navbar-inverse navbar-static-top">
                        <div class="container-fluid">
                    
                    
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <header class="navbar-header">
                    
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse ">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <?php echo $this->Html->link('Test', ['controller'=>'Users', 'action'=>'index'], ['class'=>'logo']);?>
                    
                            </header>
                    
                    
                    
                            
                    
                    
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                    
                                <!-- .nav -->
                                <ul class="nav navbar-nav">
                                    <li><?php echo $this->Html->link('Dashboard', ['controller'=>'Users', 'action'=>'index']);?></li>
                                    <li class='dropdown '>
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            Admin Panel <b class="caret"></b>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><?php echo $this->Html->link('Add Admin', ['controller'=>'Users', 'action'=>'add']);?></li>
                                            <li><?php echo $this->Html->link('Edit Admin', ['controller'=>'Users', 'action'=>'edit']);?></li>
											
                                        </ul>
                                    </li>
                                </ul>
                                <!-- /.nav -->
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                    <!-- /.navbar -->
                       
                </div>
                <!-- /#top -->
                    <div id="left">
                        <div class="media user-media bg-dark dker">
                            <div class="user-media-toggleHover">
                                <span class="fa fa-user"></span>
                            </div>
                            <div class="user-wrapper bg-dark">
                                <a class="user-link" href="">
								<?php if(empty($userinfo['Admin']['image'])){
									$userinfo['Admin']['image'] = 'user.jpg';
									}
								?>	
									<?php echo $this->Html->image('/img/'.$userinfo['Admin']['image'], array('width'=>'60px',"class"=>"media-object img-thumbnail user-img", "alt"=>"User Picture"));?>
                                </a>
                        
                                <div class="media-body">
                                    <span class="glyphicon glyphicon-user"></span> Hi, <?php echo $userinfo['Admin']['name'];?>
                                    <ul class="list-unstyled user-info">
                                        <li><span class="glyphicon glyphicon-cog"></span><?php echo $this->Html->link(' My Account', ['controller'=>'Users', 'action'=>'myaccount'],['id'=>'account_link']);?> </li>
										<li><span class="glyphicon glyphicon-share-alt"></span><?php echo $this->Html->link(' logout', ['controller'=>'Users', 'action'=>'logout'],['id'=>'account_link']);?> </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- #menu -->
                        <ul id="menu" class="bg-blue dker">
                            <li class="nav-header">Menu</li>
                                <li class="nav-divider"></li>
                                  <li class="">
                                    <?php echo $this->Html->link('Dashboard', ['controller'=>'Users', 'action'=>'index']);?>
                                    
                                  </li>
                                  <li class="">
                                    <?php echo $this->Html->link('New Customer', ['controller'=>'Customers', 'action'=>'add']);?>
                                  </li>
								  <li class="">
                                    <?php echo $this->Html->link('Customers', ['controller'=>'Customers', 'action'=>'customers']);?>
                                  </li>
								  <li class="">
                                    <?php echo $this->Html->link('Transaction', ['controller'=>'Transactions', 'action'=>'transactions']);?>
                                  </li>
								  
								  
								  
                        </ul>
                        <!-- /#menu -->
                    </div>
                    <!-- /#left -->
                <div id="content">
                    <div class="outer">
                        <div class="inner bg-light lter" style="min-height:650px;">
							<div class="row">
								<div class="col-lg-9">
									<div class="box dark">
										<h3>Total Transactions Between Jan and December <?php echo date('Y'); ?></h3>
										<div id="myDiv"></div>
										<h3>Number of Transactions Between Jan and December <?php echo date('Y'); ?></h3>
										<div id="pieDiv"></div>
										
										
									</div>
								</div>
							</div>


						</div>

					</div>
				</div>
			</div>	
            <!-- /#wrap -->
						<footer class="Footer bg-dark dker">
							
						</footer>
				
            </div>
            <!-- /.modal -->
            <!-- /#helpModal -->
           
                    

                

        </body>

</html>

<script>
  $( function() {
    $( "#date" ).datepicker({ maxDate: new Date});
  } );
 </script>
<script>
    var data = [{
	  x: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
	  y: [<?php echo $jancount; ?>, <?php echo $febcount; ?>, <?php echo $marcount; ?>, <?php echo $aprcount; ?>,<?php echo $maycount; ?>,<?php echo $juncount; ?>,<?php echo $julcount; ?>,<?php echo $augcount; ?>,<?php echo $sepcount; ?>,<?php echo $octcount; ?>,<?php echo $novcount; ?>, <?php echo $deccount; ?>],
	  type: 'bar'
	}];

	Plotly.newPlot('myDiv', data);
	
	var pieData = [{
	  values: [<?php echo $jan; ?>, <?php echo $feb; ?>, <?php echo $mar; ?>, <?php echo $apr; ?>,<?php echo $may; ?>,<?php echo $jun; ?>,<?php echo $jul; ?>,<?php echo $aug; ?>,<?php echo $sep; ?>,<?php echo $oct; ?>,<?php echo $nov; ?>, <?php echo $dec; ?>],
	  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
	  type: 'pie'
	}];

	Plotly.newPlot('pieDiv', pieData);
</script>