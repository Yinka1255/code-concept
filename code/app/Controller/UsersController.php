<?php
	
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class UsersController extends AppController {
		public $helpers = array('Html', 'Form', 'Flash', 'Javascript', 'Js');

	public $components = array('Flash', 'Session', 'Paginator', 'RequestHandler');
	
	public $theme="Admintheme";
	
	public $paginate = array(
		'limit' => 1,
		'order' => array(
		'User.id' => 'asc'));
	
	public function index() {
	
		$this->layout = 'index';
		
		$this->loadModel("Transaction");
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		$this->set('userinfo', $userinfo);
		
		$date = date("Y");
		$jan_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-01-01', 'Transaction.date <= ' => $date.'-01-31']]);
		$feb_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-02-01', 'Transaction.date <= ' => $date.'-02-28']]);
		$mar_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-03-01', 'Transaction.date <= ' => $date.'-03-31']]);
		$apr_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-04-01', 'Transaction.date <= ' => $date.'-04-30']]);
		$may_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-05-01', 'Transaction.date <= ' => $date.'-05-31']]);
		$jun_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-06-01', 'Transaction.date <= ' => $date.'-06-30']]);
		$jul_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-07-01', 'Transaction.date <= ' => $date.'-07-31']]);
		$aug_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-08-01', 'Transaction.date <= ' => $date.'-08-31']]);
		$sep_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-09-01', 'Transaction.date <= ' => $date.'-09-30']]);
		$oct_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-10-01', 'Transaction.date <= ' => $date.'-10-30']]);
		$nov_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-11-01', 'Transaction.date <= ' => $date.'-11-30']]);
		$dec_count = $this->Transaction->find('all', ['conditions' => ['Transaction.date >= ' => $date.'-12-01', 'Transaction.date <= ' => $date.'-12-31']]);
		$this->set('jan_count', $jan_count);$this->set('feb_count', $feb_count);$this->set('mar_count', $mar_count);$this->set('apr_count', $apr_count);$this->set('may_count', $may_count);
		$this->set('jun_count', $jun_count);$this->set('jul_count', $jul_count);$this->set('aug_count', $aug_count);$this->set('sep_count', $sep_count);$this->set('oct_count', $oct_count);
		$this->set('nov_count', $nov_count);$this->set('dec_count', $dec_count);
	}

	public function edit() {
	
		$this->layout = 'index';
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		$this->set('userinfo', $userinfo);
		
		$users = $this->User->find('all');
		
		$this->set('users', $users);
	
	}
	
	public function status($user_id, $status) {
		
		$this->request->data['User']['id'] = $user_id;
		
		$this->request->data['User']['status'] = $status;		
		
		if ($this->User->save($this->request->data)) {
		
			if($status ==1){
				$action = 'Activated';
			}
			else
			{
				$action = 'Deactivated';
			}	
				
			
			$this->Session->setFlash(__('User was successfully '. $action, null), 'default', array('class' => 'flash-message-success'));
		
			return $this->redirect($this->referer());
		}	
		
	}	
	
	public function add() {
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
	
		$this->layout = 'index';
		
		$this->loadModel("Admin");
	
		if ($this->request->is('post')) {

			$this->User->create();
			
			
			if ($this->User->save($this->request->data)) {
				
				$lastInsertedData = $this->User->find('first', ['order'=>['User.id'=> 'DESC']]);
				
				$lastInsertedId = $lastInsertedData['User']['id'];
				
				$this->Admin->create();
			
				$this->request->data['Admin']['name'] = $this->request->data['User']['name'];
				
				$this->request->data['Admin']['user_id'] = $lastInsertedId;
				
				$this->request->data['User']['admin_id'] = $lastInsertedId;
				
				$this->Admin->save($this->request->data);
				
				$this->User->save($this->request->data);
	
				$this->Session->setFlash(__('Admin was successfully saved', null), 'default', array('class' => 'flash-message-success'));
	
				return $this->redirect(array('controller' =>'Customers', 'action' => 'add'));
			} 
			else {
					$this->Session->setFlash(__('Oops! An error occured', null), 'default', array('class' => 'flash-message-error'));
			} 

		}
	}	


	
	public function login() {
	
		$this->layout = 'login';
		
		if($this->request->is('post')){
			if ($this->Auth->login()) {
				
				$userinfo=$this->User->findByUsername($username);
				
				if($userinfo['User']['status'] = 0){
					$this->Session->setFlash(__('Sorry! Your account has been deactivated', null), 'default', array('class' => 'flash-message-error'));
				}	
				$this->Session->setFlash(__('Login Successful', null), 'default', array('class' => 'flash-message-success'));
				return $this->redirect($this->Auth->redirectUrl());
			}

			else {
				$this->Session->setFlash(__('Login Not Successful', null), 'default', array('class' => 'flash-message-error'));
			}
		}	
	}
	
	public function myaccount() {
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
	
		$this->layout = 'index';
		
		$this->loadModel("Admin");
		
		if($this->request->is('post')){
			
			if(!empty($this->request->data['User']['image'])){
				$this->request->data['Admin']['id']=$this->request->data['User']['id'];
				move_uploaded_file($this->request->data['User']['image']['tmp_name'], WWW_ROOT . 'img/'.$this->request->data['User']['image']['name']);$this->request->data['Admin']['image']=$this->request->data['User']['image']['name'];
				if ($this->Admin->save($this->request->data)) {
				
					$this->Session->setFlash(__('Upload Was Successful!', null), 'default', array('class' => 'flash-message-success'));
					return $this->redirect($this->referer());
				}
				else {
					$this->Session->setFlash(__('Error! upload not successful', null), 'default', array('class' => 'flash-message-error'));
				}
			}	
			if ($this->User->save($this->request->data)) {
				
				$this->Session->setFlash(__('Update Was Successful!', null), 'default', array('class' => 'flash-message-success'));
				return $this->redirect($this->referer());
			}

			else {
				$this->Session->setFlash(__('Error! update not successful', null), 'default', array('class' => 'flash-message-error'));
			}
		}	
	}	
	
	public function logout() {

		return $this->redirect($this->Auth->logout());

	}
		 
		 
			
		
}