<?php
	
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class CustomersController extends AppController {
	public $helpers = array('Html', 'Form', 'Flash', 'Javascript', 'Js');

	public $components = array('Flash', 'Session', 'Paginator', 'RequestHandler');
	
	public $theme="Admintheme";
	
	
	
	
		
	public function add() {
	
		$this->loadModel('Location');
		
		$this->loadModel('User');
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
		
		$this->layout = 'index';
		
		
		$state = $this->Location->find('list', array('order'=>array('Location.state'=> 'ASC'), 'fields'=> array('Location.state', 'Location.state')));
		
		$this->set('state', $state);
	
		if ($this->request->is('post')) {


			$this->Customer->create();
			
			if(!empty($this->request->data['Customer']['image'])){
				move_uploaded_file($this->request->data['Customer']['image']['tmp_name'], WWW_ROOT . 'img/'.$this->request->data['Customer']['image']['name']);$this->request->data['Customer']['image']=$this->request->data['Customer']['image']['name'];
			}
			
			if ($this->Customer->save($this->request->data)) {
	
				$this->Session->setFlash(__('Customer information was successfully saved', null), 'default', array('class' => 'flash-message-success'));
	
				return $this->redirect(array('controller' =>'Customers', 'action' => 'add'));
			} 
			else {
					$this->Session->setFlash(__('Unable to save information', null), 'default', array('class' => 'flash-message-error'));
	
			} 

		}
	}	
	
	public function customers(){
		
		$this->loadModel('User');
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
		
		
		$this->layout = 'index';
		
		$customers = $this->Customer->find('all');
		
		$this->set('customers', $customers);
	}

	
	
}