<?php
	
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class TransactionsController extends AppController {
	public $helpers = array('Html', 'Form', 'Flash', 'Javascript', 'Js');

	public $components = array('Flash', 'Session', 'Paginator', 'RequestHandler');
	
	public $theme="Admintheme";
	
	
	
	
		
	public function add($customer_id = null) {
	
		$this->loadModel('User');
		
		$this->loadModel('Customer');
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
		
		$customer = $this->Customer->findById($customer_id);
		
		$customerName = $customer['Customer']['name'];
		
		$this->set('name', $customerName);
		
		$this->layout = 'index';
		
		
		if ($this->request->is('post')) {


			$this->Transaction->create();
			
			$this->request->data['Transaction']['customer_id'] = $customer_id;
			
			if ($this->Transaction->save($this->request->data)) {
	
				$this->Session->setFlash(__('Customer information was successfully saved', null), 'default', array('class' => 'flash-message-success'));
	
				return $this->redirect(array('controller' =>'Customers', 'action' => 'add'));
			} 
			else {
					$this->Session->setFlash(__('Unable to save information', null), 'default', array('class' => 'flash-message-error'));
	
			} 

		}
	}	
	
	public function transactions(){
		
		$this->loadModel('User');
		
		$this->loadModel('Customer');
		
		$username = $this->Session->read('Auth.User.username');
		
		$userinfo=$this->User->findByUsername($username);
		
		$this->set('userinfo', $userinfo);
		
		
		$this->layout = 'index';
		
		$transactions = $this->Transaction->find('all');
		
		$this->set('transactions', $transactions);
	}

	
	
}