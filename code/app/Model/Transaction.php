<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class Transaction extends AppModel {
	
	public $validate = array(
		'deposit' => array(
			'required' => array(
				'rule' => 'numeric',
				'message' => 'Kindly enter a valid digit'
			),
			'maxLength' => array(
				'rule' => array('maxLength', 80),
				'message' => 'Name cannot be more than 80 characters.'
			)
		),
		'date' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Kindly enter a valid date'
			),
			'maxLength' => array(
				'rule' => array('maxLength', 80),
				'message' => 'Date cannot be more than 80 characters.'
			)
		)
	);
	
	public $belongsTo = array(
        'Customer' => array(
            'className' => 'Customer',
            'dependent' => true
        )
    );
	

}
?>
