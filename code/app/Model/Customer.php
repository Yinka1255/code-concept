<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class Customer extends AppModel {
	
	public $validate = array(
    'email' => array(
        'required' => array(
            'rule' => array('email'),
            'message' => 'Kindly provide a valid email'
        ),
        'maxLength' => array(
            'rule' => array('maxLength', 80),
            'message' => 'Email cannot be more than 80 characters.'
        ),
        'unique' => array(
            'rule' => 'isUnique',
            'message' => 'Provided Email already exists.'
        )
    ),
	'phone' => array(
        'required' => array(
            'rule' => array('phone', '/\d{10}/', 'all'),
            'message' => 'Kindly provide a valid phone no'
        ),
        'maxLength' => array(
            'rule' => array('maxLength', 14),
            'message' => 'Phone no cannot be more than 14 characters.'
        ),
        'unique' => array(
            'rule' => 'isUnique',
            'message' => 'Provided Phone no already exists.'
        )
    ),
	
	'pin' => array(
        'required' => array(
            'rule' => 'numeric',
            'message' => 'Kindly provide a 4 digit pin'
        ),
        'maxLength' => array(
            'rule' => array('maxLength', 4),
            'message' => 'Pin cannot be more than 4 characters.'
        )        
    )
);

}
?>
