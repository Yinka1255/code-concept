<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class User extends AppModel {
	
	
	public $validate = array(
	'username' => array(
	'rule' => 'email',
	'message' => 'This field must be a valid email'
	),
	'minLength' => array(
		'rule' => array('minLength', 4),
		'message' => 'minimum of 4 characters'
	),
	'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Provided Username already exists.'

	),
	'password' => array(
	'rule' => 'notEmpty',
	'message' => 'A password is required'
	),
	'minLength' => array(
		'rule' => array('minLength', 6),
		'message' => 'Minimum of 6 characters'

	),

	'name' => array(
	'rule' => 'notEmpty',
	'message' => 'A name is required'
	),
	'minLength' => array(
		'rule' => array('minLength', 2),
		'message' => 'minimum of 2 characters'

	),
	);

	public $hasOne = array(
        'Admin' => array(
            'className' => 'Admin',
            'dependent' => true
        )
    );
	

	public function beforeSave($options = array()) {
	if (isset($this->data[$this->alias]['password'])) {
	$passwordHasher = new BlowfishPasswordHasher();
	$this->data[$this->alias]['password'] = $passwordHasher->hash(
	$this->data[$this->alias]['password']
	);
	}
	return true;
	}


}
?>
