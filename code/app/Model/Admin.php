<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class Admin extends AppModel {
	
	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Kindly provide a full name'
			),
			'maxLength' => array(
				'rule' => array('maxLength', 80),
				'message' => 'Name cannot be more than 80 characters.'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Provided Name already exists.'
			)
		)
	);
	

}
?>
